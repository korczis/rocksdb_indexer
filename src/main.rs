use std::fmt::{Debug, Display};
use std::path;

use anyhow::Result;
use rocksdb::{ColumnFamilyDescriptor, DBWithThreadMode, MultiThreaded, Options, SliceTransform};
use serde::{de::DeserializeOwned, Serialize};
use serde_derive::*;

#[derive(Debug)]
pub enum IndexType {
    r#String,
}

pub struct IndexedDb {
    db: DBWithThreadMode<MultiThreaded>,
}

#[derive(Debug)]
pub enum IndexedValueField {
    String(String),
    Number(String),
}

const INDEX_NAME_STRING: &str = "string_idx";
const INDEX_NAME_NUMBER: &str = "number_idx";

impl IndexedValueField {
    pub fn get_handle_key_pair(&self) -> (&String, &'static str) {
        match self {
            IndexedValueField::String(name) => (name, INDEX_NAME_STRING),
            IndexedValueField::Number(name) => (name, INDEX_NAME_NUMBER),
        }
    }
}

trait IndexedValueFieldValue: std::fmt::Display + std::fmt::Debug {}

pub trait IndexedValue: Serialize + DeserializeOwned {
    fn indices(&self) -> Vec<IndexedValueField>;
    fn field_by_name(&self, field_name: &str) -> Result<String>; // TODO different types, generic?
    fn index_name() -> String;
}

#[derive(Debug, Serialize, Deserialize)]
struct Book {
    title: String,
    author: String,
    page_count: u64,
    price: u64,
}

// FIXME should be a nice macro for this schema definition
impl IndexedValue for Book {
    fn indices(&self) -> Vec<IndexedValueField> {
        vec![
            IndexedValueField::String("title".to_string()),
            IndexedValueField::String("author".to_string()),
        ]
    }
    fn field_by_name(&self, field_name: &str) -> Result<String> {
        match field_name {
            "title" => Ok(self.title.clone()),
            "author" => Ok(self.author.clone()),
            _ => anyhow::bail!("Unknown field {}", field_name),
        }
    }

    fn index_name() -> String {
        "book".to_string()
    }
}

impl IndexedDb {
    pub fn new<P: AsRef<path::Path>>(path: P) -> Result<Self> {
        fn extract_field_prefix(k: &[u8]) -> &[u8] {
            let prefix = &k[..k.iter().rposition(|c| { *c == 0u8}).unwrap()];

            println!("PREFIX: {:?}", &String::from_utf8(Vec::from(prefix)));

            prefix
        }

        let mut opts = Options::default();
        opts.create_if_missing(true);
        opts.create_missing_column_families(true);

        let mut cf_opts = Options::default();

        // Prefix extractor(trim the timestamp at tail) for write cf.
        cf_opts.set_prefix_extractor(SliceTransform::create(
            "test slice transform",
            extract_field_prefix,
            None,
        ));

        let db_path = path::PathBuf::from(path.as_ref());

        let db = DBWithThreadMode::<MultiThreaded>::open_cf_descriptors(
            &opts,
            db_path,
            vec!["main_storage", INDEX_NAME_STRING, INDEX_NAME_NUMBER]
                .into_iter()
                .map(|i| {
                    ColumnFamilyDescriptor::new(i, cf_opts.clone())
                })
        )?;

        Ok(Self { db })
    }

    pub fn put<V: IndexedValue>(&mut self, key: &str, val: V) -> Result<()> {
        for field in val.indices() {
            let (field_name, cf_handle_name) = field.get_handle_key_pair();

            let index_key = format!(
                "{}\0{}\0{}",
                V::index_name(),
                field_name,
                val.field_by_name(&field_name)?
            );

            self.db.put_cf(
                &self.db.cf_handle(cf_handle_name).unwrap(),
                index_key.as_bytes(),
                key.as_bytes(),
            )?
        }
        self.db.put_cf(
            &self.db.cf_handle("main_storage").unwrap(),
            key,
            serde_json::to_vec(&val)?,
        )?;
        Ok(())
    }

    pub fn get<V: IndexedValue>(&self, key: &str) -> Result<Option<V>> {
        let raw_value = match self
            .db
            .get_cf(&self.db.cf_handle("main_storage").unwrap(), key)
        {
            Err(e) => anyhow::bail!("Could not get value, e = {:?}", e),
            Ok(bytes) => bytes,
        };

        if let Some(val) = raw_value {
            let string_value = std::str::from_utf8(&val)?.to_string();
            log::debug!("string value in get_by was {:?}", &string_value);
            let v: V = serde_json::from_str(&string_value)?; // deal with problematic serde lifetimes
            return Ok(Some(v));
        }
        Ok(None)
    }

    // TODO field_name and field_type should be one
    pub fn get_by<V: IndexedValue>(
        &self,
        field: IndexedValueField,
        field_value: &dyn Display,
    ) -> Result<Option<V>> {
        log::debug!(
            "get_by() field_type = {:?}, field_value = {}",
            field,
            field_value
        );

        let (field_name, cf) = match field {
            IndexedValueField::String(name) => (name, self.db.cf_handle(INDEX_NAME_STRING).unwrap()),
            IndexedValueField::Number(name) => (name, self.db.cf_handle(INDEX_NAME_NUMBER).unwrap()),
        };

        let index_key = format!("{}\0{}\0{}", V::index_name(), field_name, field_value);
        let key = match self.db.get_cf(&cf, &index_key.as_bytes())? {
            Some(key) => key,
            None => return Ok(None),
        };

        let key_str = std::str::from_utf8(&key)?;
        self.get::<V>(key_str)
    }

    pub fn del() {
        // remove all generated indices
        // then remove from main_storage
        todo!()
    }

    // TODO field_name and field_type should be one
    // TODO should return Iterator
    // needs a prefix iterator of {v::prefix()}{fieldname} of CF for field_type. that provides
    // keys to "main_storage" CF which can then be extracted from it
    pub fn sorted_by<V: IndexedValue>(
        &self,
        field: IndexedValueField,
    ) -> Result<Vec<V>> {
        // TODO: Create key for field
        let (field_name, cf) = match field {
            IndexedValueField::String(name) => (name, self.db.cf_handle(INDEX_NAME_STRING).unwrap()),
            IndexedValueField::Number(name) => (name, self.db.cf_handle(INDEX_NAME_NUMBER).unwrap()),
        };

        let index_key = format!("{}\0{}\0", V::index_name(), field_name);
        println!("ITERATOR KEY: {:?}",index_key);

        let iter = self.db.prefix_iterator_cf(&cf,index_key.as_bytes());
        for (key, value) in iter {
            let value_str = std::str::from_utf8(&value)?;
            println!("Saw {:?} {:?}", key, value_str);
        }

        Ok(vec![])
    }
}

fn main() -> Result<()> {
    env_logger::init();
    let mut db = IndexedDb::new("./_db")?;
    let b1 = Book {
        title: "ilias".to_string(),
        author: "homer".to_string(),
        page_count: 1000,
        price: 123,
    };

    let b2 = Book {
        title: "meno".to_string(),
        author: "plato".to_string(),
        page_count: 2000,
        price: 456,
    };

    db.put("book1", b1)?;
    db.put("book2", b2)?;

    // ----

    let b1_db = db
        .get_by::<Book>(IndexedValueField::String("title".to_string()), &"ilias")?
        .unwrap();
    assert_eq!(b1_db.page_count, 1000);

    println!("returned book was = {:?}", b1_db);
    println!(
        "returned book1 was = {:?}",
        db.get_by::<Book>(IndexedValueField::String("author".to_string()), &"homer")?
    );

    println!("returned book was = {:?}", b1_db);
    println!(
        "returned book1 was = {:?}",
        db.get_by::<Book>(IndexedValueField::Number("price".to_string()), &123)?
    );

    // ----

    // ----

    println!("direct get = {:?}", db.get::<Book>("book1")?);


    // ----

    db.sorted_by::<Book>(IndexedValueField::String("author".to_string()));

    // ----

    Ok(())
}
